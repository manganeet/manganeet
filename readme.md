# Manganeet
Manganeet is a website project for read adult manga or manhwa for free.  
Manganeet website will share a collection of manga or manhwa complete project only. We will not share if the project status still Ongoing.

Visit [https://manganeet.com](https://manganeet.com)

## Report Broken Link or Have any Idea?
If you found a broken link, or have any Idea to improve Manganeet website, please tell us by create a [new Issues](https://gitlab.com/manganeet/manganeet/issues).

## Advertise with Us 
Your ads will be shown in all pages of manganeet website.  
If you interested just contact me at manganeet@yahoo.com.

## Our Supporters
Want to support us? Your name or website will be listed at here.

- Donate via [Donation Alerts](https://www.donationalerts.com/r/xsilent_cleopatra)
- Donate via **BTC**: 1MtPkZmYEBtQz3odhubB1XpJXyHpV15dYj

**Thank You** very much for this people who already support us:
- Yahzze
- Umild7
- [ImgFo](https://imgfo.com)
- Rukia Kaneki
- Slutty_007
- VDestroyer
- XCleopatra
- 
